/*Template
//Date:
//Contest:
//Question:
#include <iostream>
using namespace std;
int main(int argc, char* argv[])
{
	return 0;
}
*/
#include <iostream>
using namespace std;
int main(int argc, char* argv[])
{
	int x=256;
	char y=(char)x;
	printf("%d",y);
	return 0;
}
/*
#include <iostream>
#include <string>
#include <vector>
using namespace std;
void Replace(string &str,string from,string to)
{
	string::size_type pos = 0;
	while (pos = str.find(from, pos), pos != string::npos) {
		str.replace(pos, from.length(), to);
		pos += to.length();
	}
	return;
}
int main(int argc, char* argv[])
{
	string s1;
	cin>>s1;
	string tmp;
	vector<string> s2;
	while (true){
		tmp.clear();
		cin>>tmp;
		if (tmp.size()==0){
			break;
		}
		if (!tmp.compare(s1)){
			tmp="["+tmp+"]";
		}else{
			string from = s1;
			string to ="="+s1+"=";
			Replace(tmp,from,to);
		}
		s2.push_back(tmp);
	}
	int i;
	int siz=s2.size();
	for (i=0;i<siz;i++){
		cout<<s2[i];
		if (i<siz-1){
			cout<<" ";
		}
	}
	cout<<endl;
	return 0;
}
/*
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int ANGLE[12][12]={
//0  1  2  3  4  5  6  7  8  9  10 11
{0 ,3 ,3 ,4 ,4 ,5 ,4 ,6 ,6 ,7 ,7 ,8 },//0
{8 ,0 ,3 ,4 ,4 ,5 ,5 ,6 ,6 ,7 ,7 ,8 },//1
{7 ,8 ,0 ,3 ,4 ,4 ,4 ,5 ,4 ,6 ,6 ,7 },//2
{7 ,8 ,8 ,0 ,3 ,4 ,4 ,5 ,5 ,5 ,6 ,7 },//3
{6 ,7 ,7 ,8 ,0 ,3 ,3 ,4 ,4 ,5 ,4 ,6 },//4
{6 ,7 ,7 ,8 ,8 ,0 ,3 ,4 ,4 ,5 ,5 ,5 },//5
{4 ,5 ,6 ,7 ,7 ,8 ,0 ,3 ,3 ,4 ,4 ,5 },//6
{5 ,6 ,6 ,7 ,7 ,8 ,8 ,0 ,3 ,4 ,4 ,5 },//7
{4 ,5 ,4 ,6 ,6 ,7 ,7 ,8 ,0 ,3 ,3 ,4 },//8
{4 ,5 ,5 ,5 ,6 ,7 ,7 ,8 ,8 ,0 ,3 ,4 },//9
{3 ,4 ,4 ,5 ,4 ,6 ,6 ,7 ,7 ,8 ,0 ,3 },//10
{3 ,4 ,4 ,5 ,5 ,5 ,6 ,7 ,7 ,8 ,8 ,0 }//11
};
int getN(int &N)
{
	int i=0;
	int s=1;
	int n=0;
	while (true){
		s*=2;
		if (s>N){
			N = N-s/2;
			return n;
		}
		n++;
	}
}

int main(int argc, char* argv[])
{
	int N;
	cin>>N;
	int n[100];
	int i=0;
	while (N>0){
		n[i++]=getN(N);
	}
	vector<int> angle;
	N=i-1;
	for (i=N;i>0;i--){
		angle.push_back(ANGLE[n[i]][n[i-1]]);
	}
	angle.push_back(ANGLE[n[0]][n[N]]);
	sort(angle.begin(),angle.end());
	int siz=angle.size();
	for (i=0;i<siz;i++){
		cout<<angle[i];
		if (i<siz-1){
			cout<<",";
		}
	}
	cout<<endl;
	return 0;
}
/*
//Date:
//Contest:
//Question:
typedef long long LL;
#include <iostream>
#include <vector>
using namespace std;
struct BALL{
	LL ball;
	int index;
	BALL(LL b,int i){
		ball=b;
		index=i;
	}
};
int main(int argc, char* argv[])
{
	int N;
	cin>>N;
	int i;
	BALL maxX(0,0),minX(10000000000,0),maxY(0,0),minY(10000000000,0);
	LL x,y,tmp;
	for (i=0;i<N;i++){
		cin>>x>>y;
		if (x<y){
			tmp=x;
			x=y;
			y=tmp;
		}
		if (maxX.ball<=x){
			maxX.ball=x;
			maxX.index=i;
		}
		if (maxY.ball<=y){
			maxY.ball=y;
			maxY.index=i;
		}
		if (minX.ball>x){
			minX.ball=x;
			minX.index=i;
		}
		if (minY.ball>y){
			minY.ball=y;
			minY.index=i;
		}
	}
	if (maxX.index != maxY.index && minX.ball!=minY.ball){
		LL v=(maxX.ball-maxY.ball)*(minX.ball-minY.ball);
		if (v<0){
			v=-v;
		}
		cout<<v<<endl;
	}else{
		LL v1 = (maxX.ball-minX.ball)*(maxY.ball-minY.ball);
		if (v1<0){
			v1=-v1;
		}
		LL v2 = (maxX.ball-minY.ball)*(maxY.ball-minX.ball);
		if (v2<0){
			v2=-v2;
		}
		if (v1<=v2){
			cout<<v1<<endl;
		}else{
			cout<<v2<<endl;
		}
	}
	return 0;
}
/*
//Date:2017.4.29
//Contest:060
//Question:B
#include <iostream>
using namespace std;
int main(int argc, char* argv[])
{
	long long A,B,C;
	cin>>A>>B>>C;
	
	if (A==B &&B==C){
		cout<<"NO"<<endl;
		return 0;	}
	if (A%2==0 && B%2==0 && C%2==1){
		cout<<"NO"<<endl;
		return 0;
	}
	if (A%2==1&&B%2==0&&C%2==0){
		cout<<"NO"<<endl;
		return 0;
	}

	long long i=1;
	int S=0;

	for (S=A;S<=A*B;S++){
		if (S%B==C){
			cout<<"YES"<<endl;
			return 0;
		}
	}
	cout<<"NO"<<endl;
	return 0;
}
/*
#include <iostream>
#include <vector>
using namespace std;
int main(int argc, char* argv[])
{
	int N;
	long long W;
	cin>>N>>W;
	int i;
	vector<long long> w(N,0),v(N,0);
	long long MinW=10e9;
	long long W1=0;
	long long V1=0;
	for (i=0;i<N;i++){
		cin>>w[i]>>v[i];
		W1+=w[i];
		V1+=v[i];
		if (MinW>w[i]){
			MinW=w[i];
		}
	}

	if (W1<=W){
		cout<<V1<<endl;
		return 0;
	}
	if (MinW>W){
		cout<<0<<endl;
		return 0;
	}

	return 0;
}
//Date:
//Contest:
//Question:
#include <iostream>
#include <vector>
using namespace std;
int main(int argc, char* argv[])
{
	int N;
	long long T;
	cin>>N>>T;
	vector<long long> t(N,0);
	int i;
	long long s=0;
	for (i=0;i<N;i++){
		cin>>t[i];
	}
	for (i=0;i<N-1;i++){
		long long x=t[i+1]-t[i];
		if (x<=T){
			s+=x;
		}else{
			s+=T;
		}
	}
	s+=T;
	cout<<s<<endl;
	return 0;
}

/*
//Date:2017.4.29
//Contest:060
//Question:A
#include <iostream>
#include <string>
using namespace std;
int main(int argc, char* argv[])
{
	string A,B,C;
	cin>>A>>B>>C;
	int aLen=A.length();
	int bLen=B.length();
	if (A[aLen-1]==B[0] && B[bLen-1]==C[0]){
		cout<<"YES"<<endl;
	}else{
		cout<<"NO"<<endl;
	}
	
	return 0;
}
/*
#include <iostream>
int Pow(int n){
	int N=1;
	for (int i=1;i<=n;i++){
		N *=2;
	}
	return N;
}
using namespace std;
int main(int argc, char* argv[])
{
	int n,a,b;
	cin>>n>>a>>b;
	cout<<Pow(n)-2<<endl;
	
	return 0;
}

/*
//Date:2017.4.22
//Contest:59
//Question:D
#include <iostream>
#include <vector>
using namespace std;
int main(int argc, char* argv[])
{
	int n;
	cin>>n;
	vector<long long> a(n,0);
	vector<long long> s(n,0);
	int i;
	for (i=0;i<n;i++){
		cin>>a[i];
	}
	long long aw=0;
	if (a[0]==0){
		if (a[1]>0){
			a[0]=-1;
		}else{
			a[0]=1;
		}
		aw++;
	}
	s[0]=a[0];
	long long x;
	for (i=1;i<n;i++){
		s[i]=s[i-1]+a[i];
		if (s[i-1]>0){
			if (s[i]<0){
				continue;
			}else{
				x=s[i]+1;
				if (x<0){
					x=-x;
				}
				aw+=x;
				a[i]=-(1+s[i-1]);
				s[i]=s[i-1]+a[i];
			}
		}else {
			if (s[i]>0){
				continue;
			}else{
				x=s[i]-1;
				if (x<0){
					x=-x;
				}
				aw+=x;
				a[i]=1-s[i-1];
				s[i]=s[i-1]+a[i];
			}			
		}
	}
	cout<<aw<<endl;
	return 0;
}
/*
#include <iostream>
using namespace std;
int main(int argc, char* argv[])
{
	long long X,Y;
	cin>>X>>Y;
	if (abs(X-Y)<=1){
		cout<<"Brown"<<endl;
	}else{
		cout<<"Alice"<<endl;
	}
	return 0;
}
/*
//Date:2017.4.22
//Contest:59
//Question:D
#include <iostream>
using namespace std;
int main(int argc, char* argv[])
{
	long long X,Y;
	cin>>X>>Y;
	bool bAlice=true;
	while(true){
		long long Max=max(X,Y);
		long long Min=min(X,Y);
		if (bAlice){
			if (Max<2){
				cout<<"Brown"<<endl;
				return 0;
			}
			for (int i=1;i<Max/2;i++){ 
			X=Max-2*i;
			Y=Min+2*i;
			}

			if (Max>4){
				X=Max-2*(Max/2);
				Y=Min+(Max/2);
			}else{
				X=Max-2;
				Y=Min+1;
			}
			bAlice=false;
		}else{
			if (Max<2){
				cout<<"Alice"<<endl;
				return 0;
			}
			if (Max>4){
				X=Max-2*(Max/2);
				Y=Min+(Max/2);
			}else{
				X=Max-2;
				Y=Min+1;
			}
			bAlice=true;
		}
	}
	return 0;
}
/*



/*

//Date:2017.4.22
//Contest:59
//Question:B
#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	string A,B;
	cin>>A>>B;
	int aLen=A.length();
	int bLen=B.length();
	if (aLen>bLen){
		cout<<"GREATER"<<endl;
		return 0;
	}else if (aLen<bLen){
		cout<<"LESS"<<endl;
		return 0;
	}else{
		int i;
		for (i=0;i<aLen;i++){
			if (A[i]>B[i]){
				cout<<"GREATER"<<endl;
				return 0;
			}else if (A[i]<B[i]){
				cout<<"LESS"<<endl;
				return 0;
			}
		}
		cout<<"EQUAL"<<endl;
	}

	return 0;
}
/*
//Date:2017.4.22
//Contest:59
//Question:A
#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	string s1,s2,s3;
	cin>>s1>>s2>>s3;
	cout<<char(s1[0]-'a'+'A')<<char(s2[0]-'a'+'A')<<char(s3[0]-'a'+'A')<<endl;
	return 0;
}
/*
#include <iostream>
#include <string>
#include <string.h>
using namespace std;
bool isSS(int m){
	if (m%2==0){
		return true;
	}
	if (m%3==0){
		return true;
	}
	for (int i=5;i<m;i+=2){
		if (m%i==0){
			return false;
		}
	}
	return true;
}
void gbn(string &s,int m,int n)
{
	int i;
	char buf[100000];
	for (i=m;i<=n;i++){
		if (isSS(i)){
			sprintf(buf,"%d",i);
			s+=buf;
		}
	}
	//cout<<s.c_str()<<endl;
}
int caculate(string s)
{
	int sLen= s.length();
	int i,j,k,x;
	char p;
	int Max=0;
	int minJ=100000;
	for (i=0;i<sLen;i++){
		p=s[i];
		x=0;
		if (minJ==100000){
			minJ=i;
		}
		for (j=sLen-1;j>minJ;j--){
			if (p==s[j]){
				if (minJ>j){
					minJ=j;
				}
				x=0;
				for (k=i;k<=minJ;k++){
					//cout<<s[k];
					x+=s[k]-'0';
				}
				//cout<<"=>"<<x<<endl;
				if (Max<x){
					Max=x;
				}
				break;
			}
		}
	}
	return Max;
}
int main(int argc, char* argv[])
{
	int m,n;
	cin>>m>>n;
	string s;
	gbn(s,m,n);
	int Max=caculate(s);
	cout<<Max<<endl;
	return 0;
}
/*
//Date:2017.4.17
//Contest:013
//Question:B
#include <iostream>
#include <set>
#include <vector>
using namespace std;
struct NODE{
	vector<int> link;
};
vector<int> path;
bool insertSet(set<int> &idSet,vector<NODE> nodes, int id)
{
	bool bFind=true;
	size_t i;
	set<int>::iterator sit;
	NODE node=nodes[id];
	sit=idSet.find(id);
	if (sit==idSet.end()){
		idSet.insert(id);
		path.push_back(id);
	}

	for (i=0;i<node.link.size();i++){
		if (bFind==false){
			break;
		}
		int lnkId=node.link[i];
		sit=idSet.find(lnkId);
		if (sit ==idSet.end()){
			idSet.insert(lnkId);
			path.push_back(lnkId);
			bFind =insertSet(idSet,nodes,lnkId);
		}else{
			if (i==node.link.size()-1){
				bFind=false;
			}
		}
	}
	return bFind;
}
int main(int argc, char* argv[])
{
	int N,M;
	cin>>N>>M;
	vector<NODE> nodes(N+1);
	int i;
	int a,b;
	for (i=0;i<M;i++){
		cin>>a>>b;
		nodes[a].link.push_back(b);
		nodes[b].link.push_back(a);
	}
	set<int> idSet;
	set<int>::iterator sit;
	bool bFind;
	int idSetSize=0;
	for (i=1;i<=N;i++){
		bFind = insertSet(idSet,nodes,i);
		idSetSize = idSet.size();
		if (idSetSize>=4){
			break;
		}
		idSet.clear();
	}
	cout<<path.size()<<endl;
	i=0;
	for (i=0;i<path.size();i++){
		cout<<path[i];
		if (i<path.size()){
			cout<<" ";
		}
	}
	cout<<endl;
	return 0;
}
/*
//Date:2017.4.17
//Contest:013
//Question:A
#include <iostream>
#include <vector>
using namespace std;
int main(int argc, char* argv[])
{
	int N;
	int i;
	cin>>N;
	vector<long long> A(N,0);

	for (i=0;i<N;i++){
		cin>>A[i];
	}
	int aw=0;
	long long Max=0;
	int add=0;
	Max=A[0];
	for (i=1;i<N;i++){
		if (Max<A[i]){
			if (add==0){
				add=1;//����
			}
			if (add==2){//����
				aw++;
				add=0;
			}
		}
		if (Max>A[i]){
			if (add==0){
				add=2;
			}
			if (add==1){
				aw++;
				add=0;
			}
		}
		Max=A[i];
	}
	cout<<aw+1<<endl;
	return 0;
}
/*
//Date:
//Contest:
//Question:
#include <iostream>
#include <string>
int  S[100005],T[100005];
using namespace std;
int main(int argc, char* argv[])
{
	string s,t;
	cin>>s;
	cin>>t;
	int i;
	S[0]=0;
	for (i=1;i<=s.length();i++){
		S[i] = S[i-1] + s[i-1]-'A'+1;
	}
	T[0]=0;
	for (i=1;i<=t.length();i++){
		T[i] = T[i-1] + t[i-1]-'A'+1;
	}
	int q;
	cin>>q;
	int a,b,c,d;
	long long A1,A2;
	for (i=0;i<q;i++){	
		cin>>a>>b>>c>>d;
		A1=S[b]-S[a-1];
		A2=T[d]-T[c-1];
		if (A1%3==A2%3){
			cout<<"YES"<<endl;
		}else{
			cout<<"NO"<<endl;
		}
	}
	return 0;
}
*/
